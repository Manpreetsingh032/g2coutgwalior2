package `in`.his.g2cout2gwalior


import `in`.his.g2cout2gwalior.dataModel.SalesNoItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList

class HorizontalAdapter :
    RecyclerView.Adapter<HorizontalAdapter.MyViewHolder?>() {
    lateinit var handleClick: HandleClicks
    private var list = ArrayList<SalesNoItem>()


    fun setList(list: ArrayList<SalesNoItem>) {
        this.list = list
        notifyDataSetChanged()
    }


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var skuItem: TextView = view.findViewById(R.id.txt_item)

        init {
            skuItem.isSelected = true
        }

    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.skuItem.text = list[position].salesNo

        if (this::handleClick.isInitialized) {
            holder.skuItem.setOnClickListener {
                handleClick.click(list[position].salesNo, list[position].status, list[position].id)
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }


}

interface HandleClicks {
    fun click(salesNo: String?, status: Int?, id: Int?)
}