package `in`.his.g2cout2gwalior

import `in`.his.g2cout2gwalior.dataModel.CartListItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cart_item.view.*
class CartListAdapter : RecyclerView.Adapter<CartListAdapter.MyViewHolder>() {
    private var itemList = ArrayList<CartListItem>()
    lateinit var deleteClick: DeleteClick

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cart_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.cart_item_name.text = itemList[position].nameOfItem
        holder.itemView.cart_hsn.text = itemList[position].hsn
        holder.itemView.cart_sales_no.text = itemList[position].salesNo
        holder.itemView.cart_barcode.text = itemList[position].barcode
        holder.itemView.cart_carton_gross_weight.text =
            itemList[position].cartonGrossWeight.toString()
        holder.itemView.sr_no.text = (position + 1).toString()
        holder.itemView.cart_weight_per_pcs.text = itemList[position].perPcsWeight.toString()
        holder.itemView.cart_item_delete.setOnClickListener {
            if (this::deleteClick.isInitialized) {
                deleteClick.clickDelete(itemList[position].id.toString(), position)
                print(itemList[position].id)
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun setList(list: ArrayList<CartListItem>, sku: String?) {
        itemList = list.filter { s -> sku == s.nameOfItem } as ArrayList<CartListItem>
        notifyDataSetChanged()
    }

    fun searchByBarcode(barcode: String) {
        itemList = itemList.filter { s -> s.barcode!!.contains(barcode) } as ArrayList<CartListItem>
        notifyDataSetChanged()
    }

}

interface DeleteClick {
    fun clickDelete(id: String, position: Int)
    fun clickSku(sku: String?)
}