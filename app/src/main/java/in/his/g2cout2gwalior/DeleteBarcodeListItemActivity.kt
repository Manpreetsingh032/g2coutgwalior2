package `in`.his.g2cout2gwalior

import `in`.his.g2cout2gwalior.apiinterface.ApiService
import `in`.his.g2cout2gwalior.apiinterface.RetroClient
import `in`.his.g2cout2gwalior.dataModel.*
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_delete_barcode_list_item.*
import kotlinx.android.synthetic.main.cart_list.*
import kotlinx.android.synthetic.main.cart_list.rv_cart_list
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeleteBarcodeListItemActivity : AppCompatActivity() {
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    lateinit var cartListAdapter: CartListAdapter
    var cartListItem = ArrayList<CartListItem>()
    var sku: String? = ""
    val c = this@DeleteBarcodeListItemActivity
    private val handleSharedPreference: HandleSharedPreference by lazy { HandleSharedPreference(c) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_barcode_list_item)

        sku = intent.extras?.get("SKU").toString()
        getCartItems()

        cartListAdapter = CartListAdapter()
        val linearLayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_cart_list.layoutManager = linearLayoutManager
        rv_cart_list.adapter = cartListAdapter
        cartListAdapter.deleteClick = object : DeleteClick {
            override fun clickDelete(id: String, position: Int) {
                val alertDialog: AlertDialog.Builder =
                    AlertDialog.Builder(this@DeleteBarcodeListItemActivity)
                alertDialog.setMessage("Are you sure you want to Delete Cart Item?")
                alertDialog.setPositiveButton(
                    "yes"
                ) { _, _ ->
                    deleteCartItem(id)
                }
                alertDialog.setNegativeButton(
                    "No"
                ) { _, _ ->

                }
                val alert: AlertDialog = alertDialog.create()
                alert.setCanceledOnTouchOutside(false)
                alert.show()
            }

            override fun clickSku(sku: String?) {

            }
        }

        search_barcode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (count != 0) {
                    cartListAdapter.searchByBarcode(s.toString())
                } else {
                    cartListAdapter.setList(cartListItem, sku)
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        submit_cart.setOnClickListener {
            val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this)
            alertDialog.setMessage("Are you sure you want to submit all items?")
            alertDialog.setPositiveButton(
                "yes"
            ) { _, _ ->
                updateAllList()
            }
            alertDialog.setNegativeButton(
                "No"
            ) { _, _ ->

            }
            val alert: AlertDialog = alertDialog.create()
            alert.setCanceledOnTouchOutside(false)
            alert.show()

        }
    }

    private fun updateAllList() {
        cartListItem = cartListItem.filter { s -> sku == s.nameOfItem } as ArrayList<CartListItem>
        /*val list = ArrayList<CartListItem>()
        list.clear()
        cartListItem.forEach {
            list.add(
                UpdateStock(
                    it.hsn,
                    it.cartonGrossWeight.toString(),
                    it.noOfPcs.toString(),
                    it.nameOfItem,
                    it.packaging.toString(),
                    it.barcode,
                    it.perPcsWeight.toString(),
                    it.userName
                )
            )
        }*/

        val call = apiService.PRODUCT_UPDATE_STOCK_LIST(sku, UpdateStockList(cartListItem))

        call.enqueue(object : Callback<ResponseUpdateStock> {
            override fun onResponse(
                call: Call<ResponseUpdateStock>,
                response: Response<ResponseUpdateStock>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.message.isNullOrEmpty()) {
                        Toast.makeText(
                            c,
                            "Something Went Wrong,Please resubmit",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        onBackPressed()
                    } else {
                        if (response.body()!!.message == "Updated") {
                            Toast.makeText(
                                c,
                                "Contact admin to Complete order",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                            onBackPressed()

                        } else {
                            Toast.makeText(
                                c,
                                "Something Went Wrong,Please resubmit",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                } else {
                    Toast.makeText(c, "Something Went Wrong,Please resubmit", Toast.LENGTH_SHORT)
                        .show()
                    onBackPressed()
                }
            }

            override fun onFailure(call: Call<ResponseUpdateStock>, t: Throwable) {
                Toast.makeText(
                    c,
                    "Please Check Your Connection and resubmit",
                    Toast.LENGTH_SHORT
                ).show()
                onBackPressed()
            }

        })

    }

    private fun getCartItems() {
        val call =
            apiService.GET_CART_LIST("out", handleSharedPreference.getUser(), ApiService.user_name)
        call.enqueue(object : Callback<ResponseCartList> {
            override fun onResponse(
                call: Call<ResponseCartList>,
                response: Response<ResponseCartList>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    cartListItem.clear()
                    if (response.body()!!.cartList!!.isNotEmpty()) {
                        cartListItem = response.body()!!.cartList as ArrayList<CartListItem>
                    }
                    cartListAdapter.setList(cartListItem, sku)
                }
            }

            override fun onFailure(call: Call<ResponseCartList>, t: Throwable) {
                if (t.toString().contains("java.io.EOFException")) {
                    print(t)
                } else {
                    try {
                        Toast.makeText(
                            this@DeleteBarcodeListItemActivity,
                            "There is Some Problem With Connection",
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: Exception) {
                    }
                }
            }
        })
    }

    private fun deleteCartItem(id: String) {
        val call = apiService.DELETE_FROM_CART_CALL(id.toLong())
        call.enqueue(object : Callback<ResponseInsertProduction> {
            override fun onResponse(
                call: Call<ResponseInsertProduction>,
                response: Response<ResponseInsertProduction>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.message!!.isNotEmpty()) {
                        getCartItems()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseInsertProduction>, t: Throwable) {
                if (t.toString().contains("java.io.EOFException")) {
                    print(t)
                } else {
                    try {
                        Toast.makeText(
                            this@DeleteBarcodeListItemActivity,
                            "There is Some Problem With Connection",
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: Exception) {
                    }
                }
            }
        })
    }

    private fun completeScanSalesNo(salesNo: String) {
        val call = apiService.RESPONSE_UPDATE_SALES_NO_CALL(salesNo, ApiService.user_name)

        call.enqueue(object : Callback<ResponseUpdateStock> {
            override fun onResponse(
                call: Call<ResponseUpdateStock>,
                response: Response<ResponseUpdateStock>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.message.isNullOrEmpty()) {
                        Toast.makeText(
                            c,
                            "Something went wrong,Please press complete button again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        if (response.body()!!.message == "Successful") {
                            onBackPressed()
                        } else {

                            Toast.makeText(
                                c,
                                "Something went wrong,Please press complete button again.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else {
                    Toast.makeText(
                        c,
                        "Something went wrong,Please press complete button again.",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }

            override fun onFailure(call: Call<ResponseUpdateStock>, t: Throwable) {
                Log.d("TAGSales", "onFailure: $t")
                Toast.makeText(
                    c,
                    "Check your network than press complete button again.",
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }

    override fun onBackPressed() {
        try {
            startActivity(Intent(this, CartActivity::class.java))
        } catch (e: Exception) {

        }
        super.onBackPressed()
    }


}