package `in`.his.g2cout2gwalior

import `in`.his.g2cout2gwalior.apiinterface.ApiService
import `in`.his.g2cout2gwalior.apiinterface.RetroClient
import `in`.his.g2cout2gwalior.dataModel.ResponseSalesNos
import `in`.his.g2cout2gwalior.dataModel.SalesNoItem
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_order.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderActivity : AppCompatActivity() {
    val c = this@OrderActivity
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    lateinit var horizontalAdapter: HorizontalAdapter
    var salesNo = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        MQtt.managerInstance().mQTTConnect(
            c,
            resources.getString(R.string.app_name)
        )

        MQtt.managerInstance().addConfirmConnection(object : ConfirmConnection {
            override fun onSuccess() {
                if (connectionStatus) {
                    Toast.makeText(c, "Broker Connected", Toast.LENGTH_SHORT).show()
                    MQtt.managerInstance().subscribeTopic(c, "number")
                    connectionStatus = false
                }
            }

            override fun onUnSuccess() {
                connectionStatus = true
                Handler(Looper.getMainLooper()).postDelayed({
                    if (isNetworkConnected()) {
                        MQtt.managerInstance().mQTTConnect(
                            c,
                            resources.getString(R.string.app_name)
                        )
                    } else {
                        MQtt.confirmConnection.onUnSuccess()
                    }
                }, 30000)
            }
        })

        MQtt.managerInstance().mqttValues = object : MqttValues {
            override fun onNumber(status: String) {
                getSalesList()
            }

            override fun onOthers(topic: String, string: String) {

            }
        }

        horizontalAdapter = HorizontalAdapter()
        val llm = LinearLayoutManager(c, LinearLayoutManager.VERTICAL, false)
        rv_sales_list.layoutManager = llm
        rv_sales_list.adapter = horizontalAdapter

        horizontalAdapter.handleClick = object : HandleClicks {
            override fun click(salesNo: String?, status: Int?, id: Int?) {
                this@OrderActivity.salesNo = salesNo!!
                startActivity(Intent(c, BarcodeActivity::class.java).putExtra("salesNo", salesNo))
                finish()
            }
        }

        pullToRefresh.setOnRefreshListener {
            getSalesList()
            pullToRefresh.isRefreshing = false
        }

        getSalesList()
    }

    private fun getSalesList() {
        val call = apiService.SALES_NOS_CALL(ApiService.user_name)

        call.enqueue(object : Callback<ResponseSalesNos> {
            override fun onResponse(
                call: Call<ResponseSalesNos>,
                response: Response<ResponseSalesNos>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.salesNo.isNullOrEmpty()) {
                        getSalesList()
                    } else {
                        horizontalAdapter.setList(response.body()!!.salesNo as ArrayList<SalesNoItem>)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseSalesNos>, t: Throwable) {
                print(t)
            }

        })


    }

    private fun isNetworkConnected(): Boolean {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connMgr.activeNetworkInfo
        return networkInfo?.isConnected == true
    }

    companion object {
        var connectionStatus = true
    }
}