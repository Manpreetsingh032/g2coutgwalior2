package `in`.his.g2cout2gwalior

import `in`.his.g2cout2gwalior.apiinterface.ApiService
import `in`.his.g2cout2gwalior.apiinterface.RetroClient
import `in`.his.g2cout2gwalior.dataModel.*
import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.barcode_scan.*
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.manual_barcode.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class BarcodeActivity : AppCompatActivity() {
    val c = this@BarcodeActivity
    private lateinit var barcodeDetector: BarcodeDetector
    lateinit var cameraSource: CameraSource
    var barcodeData = ""
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    var scanRead = true
    private lateinit var barcodeDetailDialog: Dialog
    private var data: DataItem? = DataItem()
    private var scanCount = 0
    lateinit var surfaceView: SurfaceView
    private lateinit var manualBarcodeDialog: Dialog
    private val animation: Animation by lazy {
        AnimationUtils.loadAnimation(
            c,
            R.anim.line_anim
        )
    }
    lateinit var bar: View
    private var cameraStatus = false
    private var saleNo = ""
    var barcodeCount = 0
    private val handleSharedPreference: HandleSharedPreference by lazy {
        HandleSharedPreference(c)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.barcode_scan)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        saleNo = intent?.extras?.get("salesNo").toString()

        surfaceView = findViewById(R.id.surface_view)
        bar = findViewById(R.id.bar)
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                bar.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation) {
                bar.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })

        /* complete_scan.setOnClickListener {
             if (count > 0) {
                 scanRead = false
                 completeScanSalesNo(OrderActivity.salesNo)
             } else {
                 Toast.makeText(c, "There is not any barcode scanned", Toast.LENGTH_SHORT).show()
             }
         }*/

        barcodeDetailDialog = Dialog(c)
        with(barcodeDetailDialog) {
            setContentView(R.layout.dialog)
            setCancelable(false)
            setCanceledOnTouchOutside(false)

            submit_btn.setOnClickListener {

            }

            cancel_btn.setOnClickListener {
                scanRead = true
                dismiss()
            }

        }

        manualBarcodeDialog = Dialog(c)
        with(manualBarcodeDialog) {
            setContentView(R.layout.manual_barcode)
            setCancelable(true)
            setCanceledOnTouchOutside(true)

            cancel_manual_barcode.setOnClickListener {
                dismiss()
            }

            submit_manual_barcode.setOnClickListener {
                if (manual_barcode.text.isNotEmpty()) {
                    scanRead = false
                    getBarcodeData(manual_barcode.text.toString().trim())
                    dismiss()
                } else {
                    Toast.makeText(
                        c,
                        "Please enter barcode first",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            setOnCancelListener {
                c.onResume()
            }

            setOnDismissListener {
                c.onResume()
            }
        }

        close.setOnClickListener {
            bar.clearAnimation()
            onPause()
            cameraStatus = true
            manualBarcodeDialog.show()
            manualBarcodeDialog.manual_barcode.setText("")
        }

        cart.setOnClickListener {
            try {
                startActivity(Intent(this, CartActivity::class.java))
                finish()
            } catch (e: Exception) {

            }
        }
    }

    override fun onResume() {
        super.onResume()
        barcodeDetector = BarcodeDetector.Builder(applicationContext)
            .setBarcodeFormats(Barcode.ALL_FORMATS)
            .build()

        cameraSource = CameraSource.Builder(applicationContext, barcodeDetector)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 1024)
            .setAutoFocusEnabled(true)
            .setRequestedFps(64.0f)
            .build()

        if (cameraStatus) {
            try {
                if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        c,
                        arrayOf(Manifest.permission.CAMERA),
                        1001
                    )
                    return
                }
                cameraSource.start(surfaceView.holder)

                bar.startAnimation(animation)

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }


        surfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    if (ActivityCompat.checkSelfPermission(
                            applicationContext,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            c,
                            arrayOf(Manifest.permission.CAMERA),
                            1001
                        )
                        return
                    }
                    cameraSource.start(surfaceView.holder)

                    bar.startAnimation(animation)

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            override fun surfaceChanged(
                holder: SurfaceHolder,
                format: Int,
                width: Int,
                height: Int
            ) {
                print("a")
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource.release()
                bar.clearAnimation()
            }
        })

        startBarcodeReader()
    }

    private fun submitBarcode() {
        try {
            if (data != null) {
                val call = apiService.INSERT_PRODUCTION_CART_CALL(
                    InsertCartItem(
                        null,
                        data!!.hsn.toString(),
                        data!!.cartonGrossWeight,
                        data!!.userName,
                        1,
                        data!!.nameOfItem,
                        data!!.noOfPcs,
                        data!!.packaging,
                        "out",
                        data!!.barcode,
                        data!!.perPcsWeight,
                        null,
                        saleNo, handleSharedPreference.getUser()
                    )
                )

                call.enqueue(object : Callback<ResponseInsertProduction> {
                    override fun onResponse(
                        call: Call<ResponseInsertProduction>,
                        response: Response<ResponseInsertProduction>
                    ) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                if (response.body()!!.message.isNullOrEmpty()) {
                                    Toast.makeText(
                                        this@BarcodeActivity,
                                        "Please verify again and resubmit",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    scanRead = true
                                } else {
                                    if (response.body()!!.message == "Successful") {
                                        barcodeCount++
                                        count.text = "$barcodeCount"
                                        scanRead = true
                                    } else {
                                        Toast.makeText(
                                            this@BarcodeActivity,
                                            "Already scanned, scan another item",
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                        scanRead = true
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseInsertProduction>, t: Throwable) {
                        Toast.makeText(
                            this@BarcodeActivity,
                            "Please Check Your Connection and resubmit",
                            Toast.LENGTH_SHORT
                        ).show()
                        startScan()
                    }
                })

            } else {
                Toast.makeText(
                    this,
                    "Please verify again and resubmit",
                    Toast.LENGTH_SHORT
                )
                    .show()
                startScan()
            }
        } catch (e: Exception) {
            print(e)
            startScan()
        }
    }


    private fun completed() {
        try {
            scanRead = false
            barcodeDetector.release()
            runOnUiThread {
                cameraSource.stop()
            }
            val intent = Intent(
                this@BarcodeActivity,
                OrderActivity::class.java
            )
            startActivity(intent)
            finish()
        } catch (e: Exception) {
        }
    }

    override fun onBackPressed() {
        try {
            startActivity(Intent(this, OrderActivity::class.java))
            finish()
        } catch (e: Exception) {

        }
        super.onBackPressed()

    }

    private fun startBarcodeReader() {
        try {
            if (!barcodeDetector.isOperational) {
                Log.d("TAG", "Detector dependencies not loaded yet")
            } else {
                barcodeDetector.setProcessor(object : Detector.Processor<Barcode> {
                    override fun release() {

                    }

                    override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                        if (scanRead) {
                            val barCodes = detections.detectedItems
                            if (barCodes.size() != 0) {
                                if (barCodes.valueAt(0).displayValue != barcodeData) {
                                    scanRead = false
                                    Log.d(
                                        "TAG",
                                        "receiveDetections: ${barCodes.valueAt(0).displayValue}"
                                    )
                                    runOnUiThread {
                                        Toast.makeText(
                                            this@BarcodeActivity,
                                            barCodes.valueAt(0).displayValue, Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                    scanCount++
                                    barcodeData = barCodes.valueAt(0).displayValue
                                    getBarcodeData(barcodeData)
                                }
                            }
                        }
                    }
                })
            }
        } catch (e: Exception) {
            Log.d("TAG", "$e")
        }

    }

    private fun getBarcodeData(barcodeData: String?) {
        val call = apiService.RESPONSE_BARCODE_SCAN_CALL(barcodeData, ApiService.user_name)

        call.enqueue(object : Callback<ResponseBarcodeScan> {
            override fun onResponse(
                call: Call<ResponseBarcodeScan>,
                response: Response<ResponseBarcodeScan>
            ) {

                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.data.isNullOrEmpty()) {
                        Toast.makeText(
                            c,
                            "Barcode already scanned or not found,try another",
                            Toast.LENGTH_SHORT
                        ).show()
                        scanRead = true
                    } else {
                        try {
                            data = response.body()!!.data?.get(0)
                            submitBarcode()
                            manualBarcodeDialog.dismiss()
                        } catch (e: Exception) {
                        }
                    }
                } else {
                    Toast.makeText(
                        c,
                        "Something went wrong,Please rescan",
                        Toast.LENGTH_SHORT
                    ).show()
                    scanRead = true
                }
            }

            override fun onFailure(call: Call<ResponseBarcodeScan>, t: Throwable) {
                print(t)
                Toast.makeText(
                    c,
                    "Please Check Your Connection and rescan",
                    Toast.LENGTH_SHORT
                ).show()
                scanRead = true
            }

        })
    }


    private fun showSaveDialog(list: List<DataItem?>?) {
        if (list != null) {
            barcodeDetailDialog.show()
            data = list[0]
            barcodeDetailDialog.item_barcode.text = data!!.barcode
            barcodeDetailDialog.item_name.text = data!!.nameOfItem
            barcodeDetailDialog.pieces.text = data!!.noOfPcs.toString()
            barcodeDetailDialog.packaging.text = data!!.packaging.toString()
            barcodeDetailDialog.carton_gross_weight.text = data!!.cartonGrossWeight.toString()
            barcodeDetailDialog.hsn.text = data!!.hsn.toString()
            barcodeDetailDialog.per_pcs_wt.text = data!!.perPcsWeight.toString()
        } else {
            startScan()
            barcodeDetailDialog.dismiss()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode != 1001) {
            Log.d("TAG", "Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                cameraSource.start(surfaceView.holder)
                bar.startAnimation(animation)
            } catch (e: Exception) {
                Log.d("CameraTAG", e.toString())
            }
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            cameraSource.stop()
        } catch (e: Exception) {
        }
    }

    private fun startScan() {
        Handler(Looper.getMainLooper()).postDelayed({
            scanRead = true
        }, 500)
    }

}