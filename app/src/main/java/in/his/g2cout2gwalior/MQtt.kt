package `in`.his.g2cout2gwalior

import android.content.Context
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*

class MQtt {
    //    private var ip = "192.168.1.18"
    private var ip = "18.118.36.18"
    private var port = "1883"
    private var id = ""
    lateinit var context: Context
    var connectionFailStatus = false
    private var token: IMqttToken? = null
    var disconnect = true
    private lateinit var countDownTimer: CountDownTimer
    lateinit var mqttValues: MqttValues

    fun mQTTConnect(context: Context, id: String) {
        this.context = context
        this.id = MqttClient.generateClientId()
        client = MqttAndroidClient(context.applicationContext, "tcp://$ip:$port", this.id)
        val options = MqttConnectOptions()
/*      options.userName = "pernod"
        options.password = "pernod1234".toCharArray()*/
        options.isCleanSession = true
        options.isAutomaticReconnect = true
        options.keepAliveInterval = 29
        options.connectionTimeout = 34

        try {
            token =
                client?.connect(options, context.applicationContext, object : IMqttActionListener {
                    override fun onSuccess(asyncActionToken: IMqttToken?) {
                        token = asyncActionToken
                        confirmConnection.onSuccess()
                        receiveMsgCallback()
                        checkConnection()
                    }

                    override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                        token = asyncActionToken
                        if (!connectionFailStatus) {
                            Toast.makeText(
                                context,
                                exception.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                            connectionFailStatus = true
                        }
                        if (disconnect) {
                            disConn()
                            confirmConnection.onUnSuccess()
                            Log.d("TAG", "M QTT Connection Lost Reason: $exception")
                        }
                        disconnect = true
                    }
                })
        } catch (e: Exception) {
            confirmConnection.onUnSuccess()
        }
    }

    fun subscribeTopic(c: Context, name: String) {
        try {
            token?.client?.subscribe(name, 0)
            Log.d("Subs $name ", name)
        } catch (e: Exception) {
            runCatching {
                Toast.makeText(c, "Error Subscribing Topic $name", Toast.LENGTH_SHORT).show()
            }
            Log.d("Subs $name ", e.toString())
            confirmConnection.onUnSuccess()
        }
    }

    fun receiveMsgCallback() {
        token?.client?.setCallback(object : MqttCallback {
            override fun connectionLost(cause: Throwable?) {
                Log.d("TAG", "Connection Lost Reason: $cause")
                disconnect = true
                confirmConnection.onUnSuccess()

            }

            @Throws(Exception::class)
            override fun messageArrived(topic: String, message: MqttMessage) {
                val common = String(message.payload)
                Log.d("TAG COMMON", common)
//                println(common)
                try {
                    when (topic) {
                        "number" -> {
                            Toast.makeText(context, "sales no. $common", Toast.LENGTH_SHORT).show()
                            if (this@MQtt::mqttValues.isInitialized) {
                                mqttValues.onNumber(common)
                            }
                        }

                        else -> {
                            if (this@MQtt::mqttValues.isInitialized) {
                                mqttValues.onOthers(topic, common)
                            }
                        }


                    }
                } catch (e: Exception) {
                    Log.d("TAG", "messageArrived: $topic" + e.message)
                }
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {
                Log.d("TAG", "Delivery Complete")
                if (this@MQtt::countDownTimer.isInitialized)
                    countDownTimer.cancel()
            }
        })
    }

    private fun countdownTimer() {
        Handler(Looper.getMainLooper()).post {
            countDownTimer = object : CountDownTimer(15000, 1000) {
                override fun onTick(p0: Long) {

                }

                override fun onFinish() {
                    Log.d("TAG", "onFinish: count")
                    /*  restart()
                      MainActivity.status = true*/
                }

            }
        }
    }

    fun publish(topic: String?, msg: ByteArray) {
        try {
            token?.client?.publish(topic, msg, 0, false)
            Log.d("Publish", "$topic, $msg")
        } catch (e: Exception) {
            Log.d("Pub", "$topic ,$e")
            confirmConnection.onUnSuccess()
        }
    }

    fun disConn() {
        try {
            token?.client?.disconnect(context.applicationContext, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("TAG", "Disconnect")
                    confirmConnection.onUnSuccess()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("TAG", "Failed to Disconnect")
                }
            })
            token?.client?.close()
            client = null
        } catch (e: Exception) {
            confirmConnection.onUnSuccess()
            e.printStackTrace()
        }
    }

    fun checkConnection() {
        /*Thread {
            while (true) {
                Thread.sleep(500)
                try {
                    if (token?.client?.isConnected!!) {
                        Log.d("TAG", "checkConnection: connected")
                    } else {
                        Log.d("TAG", "checkConnection: not connected")
                    }
                } catch (e: Exception) {
                    confirmConnection.onUnSuccess()
                }
            }
        }.start()*/
    }

    companion object {
        private var client: MqttAndroidClient? = null
        private var instance: MQtt? = null
        lateinit var confirmConnection: ConfirmConnection
        fun managerInstance(): MQtt {
            return if (instance == null) {
                instance = MQtt()
                instance!!
            } else {
                instance!!
            }
        }


    }

    fun addConfirmConnection(confirmConnect: ConfirmConnection) {
        confirmConnection = confirmConnect
    }

}

interface ConfirmConnection {
    fun onSuccess()
    fun onUnSuccess()
}


interface MqttValues {
    fun onNumber(status: String)
    fun onOthers(topic: String, string: String)
}