package `in`.his.g2cout2gwalior

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class HandleSharedPreference(private val c: Context) {

    init {
        sharedPreferences =
            c.applicationContext?.getSharedPreferences(preferences, Context.MODE_PRIVATE)
    }


    fun insertSku(skuList: Set<String>) {
        editor = sharedPreferences?.edit()!!
        editor.remove(skuPreference)
        editor.putStringSet(skuPreference, skuList)
        editor.apply()
        editor.commit()
    }

    fun skuList() = run {
        sharedPreferences?.getStringSet(skuPreference, emptySet())
    }

    fun insertLineNos(lineList: Set<String>) {
        editor = sharedPreferences?.edit()!!
        editor.remove(linePreference)
        editor.putStringSet(linePreference, lineList)
        editor.apply()
        editor.commit()
    }

    fun lineList() = run {
        sharedPreferences?.getStringSet(linePreference, emptySet())
    }

    fun getUser(): String? {
        return sharedPreferences?.getString(userPreference, "")
    }

    fun saveUser(user: String) {
        editor = sharedPreferences?.edit()!!
        editor.putString(userPreference, user)
        editor.apply()
        editor.commit()
    }


    companion object {
        private const val preferences = "Preference"
        private const val skuPreference = "skuPreference"
        private const val linePreference = "LinePreference"
        private const val userPreference = "UserPreference"
        private lateinit var editor: SharedPreferences.Editor
        var sharedPreferences: SharedPreferences? = null
    }
}