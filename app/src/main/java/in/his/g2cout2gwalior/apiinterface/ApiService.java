package in.his.g2cout2gwalior.apiinterface;

import in.his.g2cout2gwalior.dataModel.CheckUserLogin;
import in.his.g2cout2gwalior.dataModel.InsertCartItem;
import in.his.g2cout2gwalior.dataModel.Password;
import in.his.g2cout2gwalior.dataModel.ResponseBarcodeScan;
import in.his.g2cout2gwalior.dataModel.ResponseCartList;
import in.his.g2cout2gwalior.dataModel.ResponseInsertProduction;
import in.his.g2cout2gwalior.dataModel.ResponseLogout;
import in.his.g2cout2gwalior.dataModel.ResponseManageSession;
import in.his.g2cout2gwalior.dataModel.ResponseSalesNos;
import in.his.g2cout2gwalior.dataModel.ResponseUpdateStock;
import in.his.g2cout2gwalior.dataModel.UpdateStock;
import in.his.g2cout2gwalior.dataModel.UpdateStockList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    String user_name = "warehouse2";
    /*http://192.168.43.106:8082/login*/

    @GET("getLogin/")
    Call<Password> PASSWORD_CALL(@Query("user_name") String user_name,
                                 @Query("password") String password);

    @GET("getSalesNo/")
    Call<ResponseSalesNos> SALES_NOS_CALL(@Query("user_name") String user_name);


    @POST("updateStockData")
    Call<ResponseUpdateStock> PRODUCT_UPDATE_STOCK
            (@Query("sales_no") String salesNo, @Body UpdateStock updateStock);

    @GET("getProductWithBarcode")
    Call<ResponseBarcodeScan> RESPONSE_BARCODE_SCAN_CALL
            (@Query("barcode") String barcode,
             @Query("user_name") String user_name);

    @POST("updateSalesNo/")
    Call<ResponseUpdateStock> RESPONSE_UPDATE_SALES_NO_CALL
            (@Query("sales_no") String salesNo,
             @Query("user_name") String user_name);

    @GET("getCartList/")
    Call<ResponseCartList> GET_CART_LIST(@Query("type") String type,
                                         @Query("user_id") String user_id, @Query("user_name") String user_name);

    @POST("addProductionCart/")
    Call<ResponseInsertProduction> INSERT_PRODUCTION_CART_CALL(@Body InsertCartItem insertCartItem);

    @POST("deleteFromCart/")
    Call<ResponseInsertProduction> DELETE_FROM_CART_CALL(@Query("id") long id);

    @POST("updateStockDataList")
    Call<ResponseUpdateStock> PRODUCT_UPDATE_STOCK_LIST
            (@Query("sales_no") String salesNo, @Body UpdateStockList updateStock);

    @POST("manageSession/")
    Call<ResponseManageSession> MANAGE_SESSION_CALL(@Body CheckUserLogin checkUserLogin);

    @GET("logout/")
    Call<ResponseLogout> LOGOUT_CALL(@Query("warehouses") String warehouses,
                                     @Query("user_name") String user_name);

}
