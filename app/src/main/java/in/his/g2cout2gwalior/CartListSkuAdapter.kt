package `in`.his.g2cout2gwalior

import `in`.his.g2cout2gwalior.dataModel.SkuListItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cart_sku_item.view.*

class CartListSkuAdapter : RecyclerView.Adapter<CartListSkuAdapter.MyViewHolder>() {
    private var itemList = ArrayList<SkuListItem>()
    lateinit var deleteClick: DeleteClick

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cart_sku_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.cart_item_name_sku.text = itemList[position].nameOfItem
        holder.itemView.cart_hsn_sku.text = itemList[position].hsn
        holder.itemView.cart_order_no_sku.text = itemList[position].salesNo.toString()
        holder.itemView.cart_carton_gross_weight_sku.text =
            itemList[position].cartonGrossWeight.toString()
        holder.itemView.sr_no_sku.text = (position + 1).toString()
        holder.itemView.cart_weight_per_pcs_sku.text = itemList[position].perPcsWeight.toString()
        holder.itemView.cart_item_sku_count.text = itemList[position].count.toString()

        holder.itemView.setOnClickListener {
            if (this::deleteClick.isInitialized) {
                deleteClick.clickSku(itemList[position].nameOfItem.toString())
                print(itemList[position].nameOfItem)
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun setList(itemList: ArrayList<SkuListItem>) {
        this.itemList = itemList
        notifyDataSetChanged()
    }
}