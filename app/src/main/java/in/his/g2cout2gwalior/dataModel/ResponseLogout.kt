package `in`.his.g2cout2gwalior.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseLogout(

    @field:SerializedName("logout")
    val logout: Boolean? = null
)
