package `in`.his.g2cout2gwalior.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseManageSession(

	@field:SerializedName("checkUser")
	val checkUser: Boolean? = null
)
