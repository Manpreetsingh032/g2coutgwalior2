package `in`.his.g2cout2gwalior.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseCartList(

	@field:SerializedName("CartList")
	val cartList: List<CartListItem?>? = null
)

data class CartListItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("hsn")
	val hsn: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("sales_no")
	val salesNo: String? = null,

	@field:SerializedName("packaging")
	val packaging: Int? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("per_pcs_weight")
	val perPcsWeight: Double? = null,

	@field:SerializedName("carton_gross_weight")
	val cartonGrossWeight: Double? = null,

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("name_of_item")
	val nameOfItem: String? = null,

	@field:SerializedName("no_of_pcs")
	val noOfPcs: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("status")
	val status: Any? = null
)
