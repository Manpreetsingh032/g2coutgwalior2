package `in`.his.g2cout2gwalior.dataModel

import com.google.gson.annotations.SerializedName

data class UpdateStockList(
    @field:SerializedName("listProduction")
    val productionList: List<CartListItem>? = null
)
