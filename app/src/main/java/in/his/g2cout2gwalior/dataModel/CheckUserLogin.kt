package `in`.his.g2cout2gwalior.dataModel

import com.google.gson.annotations.SerializedName

data class CheckUserLogin(

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("warehouses")
	val warehouses: String? = null
)
